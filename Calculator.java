/**
 * @author Alec Westacott
 */
package cse360assign2;
/**
 * @method Calculator() default constructor, initializes the total int variable to 0 and the 'history' String to "0 ".
 * @method getTotal() prints the current total int variable.
 * @method add(int) adds the given int to the total int variable and amends the 'history' String.
 * @method subtract(int) subtracts the given int from the total int variable and amends the 'history' String.
 * @method multiply(int) multiplies the total int variable by the given int and amends the 'history' String.
 * @method divide(int) divides the total int variable by the given int and amends the 'history' String.
 * @method getHistory() prints the history of the entered operations.
 *
 * @param total integer variable holding the current total value of the calculations.
 * @param history String object holding the operation history.
 **/
public class Calculator {

	private int total;
	private String history;
	/**
	 * @method Calculator () default constructor for the Calculator class, initializes the 'total'
	 *  integer to 0 and the 'history' String to "0 ".
	 */
	public Calculator () {
		total = 0;  // not needed - included for clarity
		history = "0 ";
	}
	
	/**
	 * @method getTotal () returns the current value of the 'total' integer.
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 * @method add (int) adds the provided integer value to the 'total' integer variable and 
	 * amends "+ (value) " onto the 'history' String.
	 * 
	 * @param value integer parameter to be added to the 'total' integer variable.
	 */
	public void add (int value) {
		history = history + "+ " + value + " ";
		total = total + value;
	}
	
	/**
	 * @method subtract (int) subtracts the provided integer value from the 'total' integer 
	 * variable and amends "- (value) " onto the 'history' String.
	 * 
	 * @param value integer parameter to be subtracted from the 'total' integer variable.
	 */
	public void subtract (int value) {
		history = history + "- " + value + " ";
		total = total - value;
	}
	
	/**
	 * @method multiply (int) multiplies the 'total' integer variable by the provided integer value
	 * and amends "* (value) " onto the 'history' String.
	 * 
	 * @param value integer parameter for the 'total' integer variable to be multiplied by.
	 */
	public void multiply (int value) {
		history = history + "* " + value + " ";
		total = total * value;
	}
	
	/**
	 * @method divide (int) divides the 'total' integer variable by the provided integer value.
	 * and or sets the 'total' to 0 if a 0 is entered amends "/ (value) " 
	 * onto the 'history' String.
	 * 
	 * @param value integer parameter used to divide the 'total' integer variable.
	 */
	public void divide (int value) {
		if(value == 0){
			total = 0;
			history = history + "/ " + value + " ";
		}
		else {
			history = history + "/ " + value + " ";
			total = total / value;
		}
	}
	
	/**
	 * @method getHistory () returns the 'history' String object which holds a log of the entered operations.
	 */
	public String getHistory () {
		return history;
	}
}