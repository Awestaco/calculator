/**
 * @author Alec Westacott
 */
package cse360assign2;
/**
 * @method Calculator() default constructor, initializes the total int variable to 0.
 * @method getTotal() prints the current total int variable.
 * @method add(int) adds the given int to the total int variable.
 * @method subtract(int) subtracts the given int from the total int variable.
 * @method multiply(int) multiplies the total int variable by the given int.
 * @method divide(int) divides the total int variable by the given int.
 * @method getHistory() returns a blank string.
 *
 * @param total integer variable holding the current total value of the calculations.
 **/
public class Calculator {

	private int total;
	
	/**
	 * @method Calculator () default constructor for the Calculator class, initializes the 'total'
	 *  integer to 0..
	 */
	public Calculator () {
		total = 0;  // not needed - included for clarity
	}
	
	/**
	 * @method getTotal () returns the current value of the 'total' integer.
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 * @method add (int) adds the provided integer value to the 'total' integer variable.
	 * 
	 * @param value integer parameter to be added to the 'total' integer variable.
	 */
	public void add (int value) {
		total = total + value;
	}
	
	/**
	 * @method subtract (int) subtracts the provided integer value from the 'total' integer.
	 * 
	 * @param value integer parameter to be subtracted from the 'total' integer variable.
	 */
	public void subtract (int value) {
		total = total - value;
	}
	
	/**
	 * @method multiply (int) multiplies the 'total' integer variable by the provided integer value
	 * and amends "* (value) " onto the 'history' String.
	 * 
	 * @param value integer parameter for the 'total' integer variable to be multiplied by.
	 */
	public void multiply (int value) {
		total = total * value;
	}
	
	/**
	 * @method divide (int) divides the 'total' integer variable by the provided integer value.
	 * and or sets the 'total' to 0 if a 0 is entered.
	 * 
	 * @param value integer parameter used to divide the 'total' integer variable.
	 */
	public void divide (int value) {
		if(value == 0){
			total = 0;
		}
		else {
			total = total / value;
		}
	}
	
	/**
	 * @method getHistory () returns an empty string.
	 */
	public String getHistory () {
		return "";
	}
}