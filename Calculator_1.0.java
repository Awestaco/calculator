/**
 * @author Alec Westacott
 */
package cse360assign2;
/**
 * @method Calculator() default constructor, initializes the total int variable to 0.
 * @method getTotal() empty.
 * @method add(int) empty.
 * @method subtract(int) empty.
 * @method multiply(int) empty.
 * @method divide(int) empty.
 *
 * @param total integer variable holding the current total value of the calculations.
 **/
public class Calculator {

	private int total;
	
	public Calculator () {
		total = 0;  // not needed - included for clarity
	}
	
	public int getTotal () {
		return 0;
	}
	
	public void add (int value) {
		
	}
	
	public void subtract (int value) {
		
	}
	
	public void multiply (int value) {
		
	}
	
	public void divide (int value) {
		
	}
	
	public String getHistory () {
		return "";
	}
}